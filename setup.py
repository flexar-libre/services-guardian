"""Paquete para monitorización de servicios"""

from setuptools import setup, find_packages


setup(
    name='services-guardian',
    version='0.0.1-dev',
    description=__doc__,
    author='Flexar S.R.L.',
    author_email='sistemas@flexar.com.ar',
    maintainer='Flexar S.R.L.',
    maintainer_email='sistemas@flexar.com.ar',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    classifiers=[
        'Natural Language :: Spanish',
        'License :: OSI Approved :: GNU General Public License (GPL)',
        'Operating System :: OS Independent',
        'Intended Audience :: Developers',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3.10',
        'Programming Language :: Python :: 3.11',
    ],
    python_requires='>=3.9',
    platforms='any',
    license='GPL-3',
    install_requires=[],
    tests_require=[],
)
